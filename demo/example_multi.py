# Select Multiple Items
from Menu import Menu

menu = Menu(["Milk", "Eggs", "Cheese", "Cereal", "Bread"],
            title="Grocery List", post_text="Enter a comma-separated list of items",
            prompt="What do you need?")
menu.choose_multiple()
print(f"You chose: {menu.choice_texts}")
