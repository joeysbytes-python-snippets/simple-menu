import pytest
import data
from Menu import Menu


def test_eq_equal_defaults():
    menu_1 = Menu(data.ITEMS_INPUT)
    menu_2 = Menu(data.ITEMS_INPUT)
    assert menu_1 == menu_2
    assert not menu_1 != menu_2


def test_eq_equal_all_properties():
    menu_1 = Menu(data.ITEMS_INPUT,
                  title=data.TITLE,
                  prompt=data.PROMPT,
                  pre_text=data.PRE_TEXT,
                  post_text=data.POST_TEXT)
    menu_2 = Menu(data.ITEMS_INPUT,
                  title=data.TITLE,
                  prompt=data.PROMPT,
                  pre_text=data.PRE_TEXT,
                  post_text=data.POST_TEXT)
    assert menu_1 == menu_2
    assert not menu_1 != menu_2


def test_ne_items():
    items = list(data.ITEMS_INPUT)
    items.append("abc")
    items = tuple(items)
    menu_1 = Menu(items)
    menu_2 = Menu(data.ITEMS_INPUT)
    assert menu_1 != menu_2
    assert not menu_1 == menu_2


def test_ne_title():
    menu_1 = Menu(data.ITEMS_INPUT,
                  title=data.TITLE)
    menu_2 = Menu(data.ITEMS_INPUT)
    assert menu_1 != menu_2
    assert not menu_1 == menu_2


def test_ne_prompt():
    menu_1 = Menu(data.ITEMS_INPUT,
                  prompt=data.PROMPT)
    menu_2 = Menu(data.ITEMS_INPUT)
    assert menu_1 != menu_2
    assert not menu_1 == menu_2


def test_ne_pre_text():
    menu_1 = Menu(data.ITEMS_INPUT,
                  pre_text=data.PRE_TEXT)
    menu_2 = Menu(data.ITEMS_INPUT)
    assert menu_1 != menu_2
    assert not menu_1 == menu_2


def test_ne_post_text():
    menu_1 = Menu(data.ITEMS_INPUT,
                  post_text=data.POST_TEXT)
    menu_2 = Menu(data.ITEMS_INPUT)
    assert menu_1 != menu_2
    assert not menu_1 == menu_2


def test_ne_different_types():
    menu_1 = Menu(data.ITEMS_INPUT)
    menu_2 = "some string"
    assert menu_1 != menu_2
    assert not menu_1 == menu_2


def test_ne_none_types():
    menu_1 = Menu(data.ITEMS_INPUT)
    menu_2 = None
    assert menu_1 != menu_2
    assert not menu_1 == menu_2
