import pytest
import data
from Menu import Menu


def build_str(items=data.ITEMS_STORED,
              title=None,
              prompt="Enter choice",
              pre_text=None,
              post_text=None,
              choice_index=None,
              choice_number=None,
              choice_text=None,
              choice_indexes=None,
              choice_numbers=None,
              choice_texts=None) -> str:
    str_text = ""
    str_text += "Title:              " + str(title) + "\n"
    str_text += "Pre-Text:           " + str(pre_text) + "\n"
    str_text += "Menu Items:         " + str(items) + "\n"
    str_text += "Post-Text:          " + str(post_text) + "\n"
    str_text += "Prompt:             " + str(prompt) + "\n"
    str_text += "Choice Index:       " + str(choice_index) + "\n"
    str_text += "Choice Number:      " + str(choice_number) + "\n"
    str_text += "Choice Text:        " + str(choice_text) + "\n"
    str_text += "Choice Indexes:     " + str(choice_indexes) + "\n"
    str_text += "Choice Numbers:     " + str(choice_numbers) + "\n"
    str_text += "Choice Texts:       " + str(choice_texts)
    return str_text


def test_default():
    expected = build_str()
    menu = Menu(data.ITEMS_INPUT)
    actual = str(menu)
    assert actual == expected


def test_all_properties(monkeypatch):
    expected = build_str(items=data.ITEMS_STORED,
                         title=data.TITLE,
                         prompt=data.PROMPT,
                         pre_text=data.PRE_TEXT,
                         post_text=data.POST_TEXT,
                         choice_index=0,
                         choice_number=1,
                         choice_text="-1",
                         choice_indexes=data.VALID_MULTI_CHOICE_INDEXES,
                         choice_numbers=data.VALID_MULTI_CHOICE_NUMBERS,
                         choice_texts=data.VALID_MULTI_CHOICE_TEXTS)
    menu = Menu(data.ITEMS_INPUT,
                title=data.TITLE,
                prompt=data.PROMPT,
                pre_text=data.PRE_TEXT,
                post_text=data.POST_TEXT)
    menu.CONFIG["ansi_support"] = False
    monkeypatch.setattr('builtins.input', lambda _: data.VALID_MULTI_CHOICE_INPUT)
    menu.choose_multiple(duplicates=True, sort=False)
    actual = str(menu)
    assert actual == expected
