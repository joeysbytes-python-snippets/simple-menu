import pytest
import data
from Menu import Menu


@pytest.fixture(autouse=True)
def setup_and_teardown():
    Menu.CONFIG["ansi_support"] = False
    menu = Menu(data.ITEMS_INPUT)
    yield menu
    Menu.CONFIG["ansi_support"] = True


def test_choice_nothing_chosen(setup_and_teardown):
    menu = setup_and_teardown
    assert menu.choice_index is None
    assert menu.choice_number is None
    assert menu.choice_text is None
    assert menu.choice_indexes is None
    assert menu.choice_numbers is None
    assert menu.choice_texts is None


def test_choice_valid_single(setup_and_teardown):
    menu = setup_and_teardown
    user_index = 1
    menu._choice_indexes = (user_index, )
    assert menu.choice_index == user_index
    assert menu.choice_number == user_index + 1
    assert menu.choice_text == data.ITEMS_STORED[user_index]
    assert menu.choice_indexes == (user_index, )
    assert menu.choice_numbers == (user_index + 1, )
    assert menu.choice_texts == (data.ITEMS_STORED[user_index], )


def test_choice_valid_multiple(setup_and_teardown):
    menu = setup_and_teardown
    user_indexes = (2, 3, 0)
    menu._choice_indexes = user_indexes
    assert menu.choice_index == user_indexes[0]
    assert menu.choice_number == user_indexes[0] + 1
    assert menu.choice_text == data.ITEMS_STORED[user_indexes[0]]
    assert menu.choice_indexes == user_indexes
    assert id(menu.choice_indexes) != id(user_indexes)
    assert menu.choice_numbers == tuple([idx + 1 for idx in user_indexes])
    assert menu.choice_texts == tuple([data.ITEMS_STORED[idx] for idx in user_indexes])
