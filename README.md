# Simple Python Console Menu

[[_TOC_]]

## Description

This is a menu class that can be used with a minimal of effort, but also has some very convenient features, such as:

* User can make a single choice
* User can make a comma-separated list of choices
  * Duplicates can be enabled / disabled
  * Sorting can be enabled / disabled
* ANSI color and attribute support can be enabled / disabled at a class level

## Quick Start

**Code:**

```python
# Select Single Item
from Menu import Menu

menu = Menu(["Linux", "Windows", "MacOS"], title="Operating Systems", prompt="Choose your operating system")
menu.choose()
print(f"You chose: {menu.choice_text}")
```

**Output:**

```text
Operating Systems

  1) Linux
  2) Windows
  3) MacOS

Choose your operating system: 1
You chose: Linux
```

**Code:**

```python
# Select Multiple Items
from Menu import Menu

menu = Menu(["Milk", "Eggs", "Cheese", "Cereal", "Bread"],
            title="Grocery List", post_text="Enter a comma-separated list of items",
            prompt="What do you need?")
menu.choose_multiple()
print(f"You chose: {menu.choice_texts}")
```

**Output:**

```text
Grocery List

  1) Milk
  2) Eggs
  3) Cheese
  4) Cereal
  5) Bread

Enter a comma-separated list of items

What do you need?: 4,1,1,5
You chose: ('Milk', 'Cereal', 'Bread')
```

## Installation

Currently Simple Menu is not published to a Python package repository (such as PyPI). Just copy Menu.py to any package
you want for your project. 

## Menu Components

A menu is composed of 5 parts. Only the menu items are required, all other components are optional.

```text
Operating Systems                                        <-- title

Below is a list of operating systems to choose from.     <-- pre-text

  1) Linux                                               <-- menu items
  2) Windows
  3) MacOS
  4) BSD

Choose which OS to get installation instructions for.    <-- post-text

Enter Choice:                                            <-- prompt
```

## Usage

### Create a menu instance

```python
Menu(items: Sequence[str], 
     title: Optional[str] = None, 
     prompt: Optional[str] = "Enter choice",
     pre_text: Optional[str] = None, 
     post_text: Optional[str] = None)
```

**Parameters:**

| Parameter | Valid Values        | Default Value | Description                                                                       |
|-----------|---------------------|---------------|-----------------------------------------------------------------------------------|
| items     | Sequence of strings | n/a           | Tuple, List, or other sequence of menu items. All values are converted to strings |
| title     | String / None       | None          | Title of menu                                                                     |
| prompt    | String / None       | Enter choice  | Text of prompt for user to enter their selection(s)                               | 
| pre_text  | String / None       | None          | Text printed after menu title, and before list of menu items                      |
| post_text | String / None       | None          | Text printed after menu items, and before prompt                                  |                                 |

### Choose item(s)

Invoking the *choose()* or *choose_multiple()* method does not return any values, but instead sets the 
appropriate menu properties which you can then query.

**choose_multiple() Parameters:**

| Parameter  | Valid Values | Default Value | Description                                                  |
|------------|--------------|---------------|--------------------------------------------------------------|
| duplicates | True / False | False         | If false, duplicate entries will be removed from user input. |
| sort       | True / False | True          | If true, user entries will be sorted by menu item order.     |

## Properties

These properties can be changed as needed:

| Property   | Description                                                                                                          |
|------------|----------------------------------------------------------------------------------------------------------------------|
| menu_items | Copy of: Tuple of strings which are the menu items. If this property is changed, the choice properties will be reset |
| title      | Title of menu                                                                                                        |
| prompt     | Text of user prompt                                                                                                  |
| pre_text   | Text displayed after menu title, and before menu items                                                               |
| post_text  | Text displayed after menu items, and before prompt                                                                   |

These properties are updated as *choose()* or *choose_multiple()* is invoked:

| Property        | Description                                                                                                                                   |
|-----------------|-----------------------------------------------------------------------------------------------------------------------------------------------|
| choice_index    | Index of user-selected menu item. If user selected more than 1, the first index is returned. Returns *None* if user has not made a selection. |
| choice_number   | Same as choice index, but is the number the user actually typed in.                                                                           |
| choice_text     | Text of user-selected menu item. If user selected more than 1, the first text is returned. Returns *None* if user has not made a selection.   |
| choices_indexes | Copy of: Tuple of user-selected menu indexes. Returns *None* if user has not made a selection.                                                |
| choice_numbers  | Same as choice indexes, but are the numbers the user actually typed in.                                                                       |
| choices_texts   | Tuple of user-selected menu texts. Returns *None* if user has not made a selection.                                                           |

## Advanced Usage

### Configuration Options

At the class level is a dictionary called *CONFIG*. The values in this dictionary can be changed to adjust menu
behavior. Note that there is no validations if you change these values.

| Key                       | Valid Values | Default Value | Description                                                                                                       |
|---------------------------|--------------|---------------|-------------------------------------------------------------------------------------------------------------------|
| ansi_support              | True / False | True          | If ANSI support is enabled, the menu will use the THEME (color, underline, etc) in its output                     |
| error_count_redisplay     | int >= 1     | 3             | If the user enters invalid values multiple times, after this count is reached, the menu is redisplayed            |
| multiple_choice_separator | character    | , (comma)     | For choose_multiple(), this changes the character used to separate entries (this is passed to str.split() method) |

### Theming

At the class level is a dictionary called *THEME*. The values in this dictionary can be changed to adjust ANSI
output. Note that there is no validations if you change these values.

[Tiny ANSI](https://gitlab.com/joeysbytes-python-snippets/tiny-ansi) is embedded in Simple Menu, and for each
key below, the *highlight* sub-key is a dictionary corresponding to the parameters for it.

| Key         | Sub-Key   | Default Value | Highlight Description                    | Sub-Key Description  |
|-------------|-----------|---------------|------------------------------------------|----------------------|
| title       |           |               | Title of menu                            |                      |
| pre_text    |           |               | Pre-text of menu                         |                      |
| item_num    | num_width | 3             | Item number                              | Width of item number |
| item_sep    | char      | ')'           | Character after item number              | Separator character  |
| item        |           |               | Item description                         |                      |
| post_text   |           |               | Post-text of menu                        |                      |
| prompt      |           |               | Prompt text                              |                      |
| prompt_sep  | char      | ':'           | Character after prompt text              | Separator character  |
| error       |           |               | ERROR notification pre-text              |                      |
| error_text  |           |               | The text of the error                    |                      |
| error_range |           |               | The valid range numbers in error message |                      |

### For Developers

The following dunder methods have been implemented for developers:

* repr (does not consider values of choice properties)
* str
* eq, ne (does not compare values of choice properties)
