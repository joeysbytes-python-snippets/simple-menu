import pytest
import data
from Menu import Menu


@pytest.fixture(autouse=True)
def setup_and_teardown():
    Menu.CONFIG["ansi_support"] = False
    menu = Menu(data.ITEMS_INPUT)
    yield menu
    Menu.CONFIG["ansi_support"] = True


def test_single_choice_error(capsys, setup_and_teardown):
    menu = setup_and_teardown
    expected = f"\n{data.ERROR_SINGLE_CHOICE}\n\n"
    menu._display_choice_error(multi_choices=False, multi_duplicates=False)
    captured = capsys.readouterr()
    assert captured.out == expected


def test_multi_choice_duplicates_error(capsys, setup_and_teardown):
    menu = setup_and_teardown
    expected = f"\n{data.ERROR_MULTI_CHOICE}\n\n"
    menu._display_choice_error(multi_choices=True, multi_duplicates=True)
    captured = capsys.readouterr()
    assert captured.out == expected


def test_multi_choice_no_duplicates_error(capsys, setup_and_teardown):
    menu = setup_and_teardown
    expected = f"\n{data.ERROR_MULTI_CHOICE_NO_DUPLICATES}\n\n"
    menu._display_choice_error(multi_choices=True, multi_duplicates=False)
    captured = capsys.readouterr()
    assert captured.out == expected
