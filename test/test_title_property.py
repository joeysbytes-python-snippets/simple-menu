import pytest
import data
from Menu import Menu


@pytest.mark.parametrize("title", (None, ""))
def test_title_none(title):
    menu = Menu(data.ITEMS_INPUT, title=title)
    assert menu.title is None


@pytest.mark.parametrize("title", (-1, True, 3.14, "Title"))
def test_title(title):
    menu = Menu(data.ITEMS_INPUT, title=title)
    assert menu.title == str(title)
