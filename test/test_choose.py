import pytest
import data
from Menu import Menu


@pytest.fixture(autouse=True)
def setup_and_teardown():
    Menu.CONFIG["ansi_support"] = False
    menu = Menu(data.ITEMS_INPUT, prompt=data.PROMPT)
    yield menu
    Menu.CONFIG["ansi_support"] = True


@pytest.mark.parametrize("user_choice", data.VALID_SINGLE_CHOICES)
def test_choose_valid(monkeypatch, setup_and_teardown, user_choice):
    menu = setup_and_teardown
    monkeypatch.setattr('builtins.input', lambda _: user_choice)
    expected = int(user_choice) - 1
    menu.choose()
    assert menu.choice_index == expected


@pytest.mark.parametrize("user_choice", data.INVALID_SINGLE_CHOICES)
def test_choose_invalid(monkeypatch, setup_and_teardown, user_choice):
    menu = setup_and_teardown
    monkeypatch.setattr('builtins.input', lambda _: user_choice)
    with pytest.raises(ValueError):
        menu._user_choose(prompt=data.PROMPT,
                          multi_choices=False,
                          multi_duplicates=False,
                          multi_sort=False)
