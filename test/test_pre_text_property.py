import pytest
import data
from Menu import Menu


@pytest.mark.parametrize("pre_text", (None, ""))
def test_pre_text_none(pre_text):
    menu = Menu(data.ITEMS_INPUT, pre_text=pre_text)
    assert menu.pre_text is None


@pytest.mark.parametrize("pre_text", (-1, True, 3.14, "Pre Text"))
def test_pre_text(pre_text):
    menu = Menu(data.ITEMS_INPUT, pre_text=pre_text)
    assert menu.pre_text == str(pre_text)
