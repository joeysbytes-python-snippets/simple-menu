import pytest
import data
from Menu import Menu


@pytest.mark.parametrize("post_text", (None, ""))
def test_post_text_none(post_text):
    menu = Menu(data.ITEMS_INPUT, post_text=post_text)
    assert menu.post_text is None


@pytest.mark.parametrize("post_text", (-1, True, 3.14, "Post Text"))
def test_post_text(post_text):
    menu = Menu(data.ITEMS_INPUT, post_text=post_text)
    assert menu.post_text == str(post_text)
