ITEMS_INPUT = (-1, "Zero", 3.14, None, True)
ITEMS_STORED = ("-1", "Zero", "3.14", "None", "True")
ITEMS_DISPLAYED = (
    "  1) -1\n" +
    "  2) Zero\n" +
    "  3) 3.14\n" +
    "  4) None\n" +
    "  5) True\n"
)

TITLE = "Number Menu"
PRE_TEXT = "Please select your favorite number(s)"
POST_TEXT = "These numbers will follow you forever"
PROMPT = "Which do you like?"

VALID_SINGLE_CHOICES = ("1", "2", "3", "4", "5")
INVALID_SINGLE_CHOICES = ("0", "6", "-1", "a", "", " ")

VALID_MULTI_CHOICE_INPUT = "1,2,2,2,5,4,4,3"
VALID_MULTI_CHOICE_INDEXES = (0, 1, 1, 1, 4, 3, 3, 2)
VALID_MULTI_CHOICE_NUMBERS = (1, 2, 2, 2, 5, 4, 4, 3)
VALID_MULTI_CHOICE_TEXTS = ("-1", "Zero", "Zero", "Zero", "True", "None", "None", "3.14")
VALID_MULTI_CHOICE_INDEXES_SORTED = (0, 1, 1, 1, 2, 3, 3, 4)
VALID_MULTI_CHOICE_INDEXES_NO_DUPES = (0, 1, 4, 3, 2)
VALID_MULTI_CHOICE_INDEXES_NO_DUPES_SORTED = (0, 1, 2, 3, 4)

ERROR_SINGLE_CHOICE = "INVALID CHOICE: Please choose a valid item between 1 and 5"
ERROR_MULTI_CHOICE = "INVALID CHOICE(S): Please choose valid item(s) between 1 and 5"
ERROR_MULTI_CHOICE_NO_DUPLICATES = "INVALID CHOICE(S): Please choose valid item(s), without duplicates, between 1 and 5"
