SHELL := bash
MAKE_DIR := ./make
SOURCE_DIR := ./src
TESTS_DIR := ./test
DEMO_DIR := ./demo


.PHONY: help
help:
	@echo ""
	@cat "$(MAKE_DIR)/help.txt"
	@echo ""


.PHONY: demo
demo:
	@export PYTHONPATH="../src/" && cd "$(SOURCE_DIR)" && python3 "../$(DEMO_DIR)/simple_menu_demo.py"


.PHONY: tests
tests:
	@export PYTHONPATH="../src/" && cd "$(SOURCE_DIR)" && python3 -m pytest "../$(TESTS_DIR)"
