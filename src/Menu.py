# MIT License: https://gitlab.com/joeysbytes-python-snippets/simple-menu/-/raw/main/LICENSE?ref_type=heads
# Copyright (c) 2024 Joey Rockhold

from typing import Sequence, Optional, Tuple, Dict
import copy


class Menu:
    # Class Variables / Constants ######################################################################################

    CONFIG = {"ansi_support": True,
              "error_count_redisplay": 3,
              "multiple_choice_separator": ","}

    THEME = {"title": {"highlight": {"fg": 11, "bg": None, "attrib": "underline"}},
             "pre_text": {"highlight": {"fg": 6, "bg": None, "attrib": None}},
             "item_num": {"highlight": {"fg": 10, "bg": None, "attrib": None}, "num_width": 3},
             "item_sep": {"highlight": {"fg": 5, "bg": None, "attrib": None}, "char": ")"},
             "item": {"highlight": {"fg": 7, "bg": None, "attrib": None}},
             "post_text": {"highlight": {"fg": 6, "bg": None, "attrib": None}},
             "prompt": {"highlight": {"fg": 10, "bg": None, "attrib": None}},
             "prompt_sep": {"highlight": {"fg": 5, "bg": None, "attrib": None}, "char": ":"},
             "error": {"highlight": {"fg": 11, "bg": 1, "attrib": None}},
             "error_text": {"highlight": {"fg": 7, "bg": None, "attrib": None}},
             "error_range": {"highlight": {"fg": 15, "bg": 4, "attrib": None}}}

    # Dunder Methods ###################################################################################################

    def __init__(self,
                 items: Sequence[str],
                 title: Optional[str] = None,
                 prompt: Optional[str] = "Enter choice",
                 pre_text: Optional[str] = None,
                 post_text: Optional[str] = None):
        self.menu_items = items
        self.title = title
        self.prompt = prompt
        self.pre_text = pre_text
        self.post_text = post_text

    def __eq__(self, other):
        if isinstance(other, Menu):
            if ((other.menu_items == self.menu_items) and
                    (other.title == self.title) and
                    (other.prompt == self.prompt) and
                    (other.pre_text == self.pre_text) and
                    (other.post_text == self.post_text)):
                return True
        return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def __repr__(self) -> str:
        text = "Menu("
        text += f"items={self.menu_items!r}"
        text += f", title={self.title!r}"
        text += f", prompt={self.prompt!r}"
        text += f", pre_text={self.pre_text!r}"
        text += f", post_text={self.post_text!r}"
        text += ")"
        return text

    def __str__(self) -> str:
        newline = '\n'
        label_format = "{: <20}"
        # title
        text = label_format.format("Title:")
        text += f"{self.title}"
        text += newline
        # pre-text
        text += label_format.format("Pre-Text:")
        text += f"{self.pre_text}"
        text += newline
        # items
        text += label_format.format("Menu Items:")
        text += str(self.menu_items)
        text += newline
        # post-text
        text += label_format.format("Post-Text:")
        text += f"{self.post_text}"
        text += newline
        # prompt
        text += label_format.format("Prompt:")
        text += f"{self.prompt}"
        text += newline
        # choice index
        text += label_format.format("Choice Index:")
        text += f"{self.choice_index}"
        text += newline
        # choice number
        text += label_format.format("Choice Number:")
        text += f"{self.choice_number}"
        text += newline
        # choice text
        text += label_format.format("Choice Text:")
        text += f"{self.choice_text}"
        text += newline
        # choice indexes
        text += label_format.format("Choice Indexes:")
        text += str(self.choice_indexes)
        text += newline
        # choice numbers
        text += label_format.format("Choice Numbers:")
        text += str(self.choice_numbers)
        text += newline
        # choice texts
        text += label_format.format("Choice Texts:")
        text += str(self.choice_texts)
        # return data
        return text

    # Properties #######################################################################################################

    @property
    def menu_items(self) -> Tuple[str, ...]:
        # tuples keep the same object id when doing a deepcopy, need to force the copy
        return tuple(list(copy.deepcopy(self._menu_items)))

    @menu_items.setter
    def menu_items(self, items: Sequence[str]) -> None:
        item_list = [str(i) for i in items]
        if len(item_list) == 0:
            raise ValueError("ERROR: Menu item list cannot be empty")
        self._menu_items = tuple(item_list)
        self._choice_indexes: Optional[Tuple[int, ...]] = None

    @property
    def title(self) -> Optional[str]:
        return self._title

    @title.setter
    def title(self, text: Optional[str]) -> None:
        self._title = Menu._get_property_text(text)

    @property
    def pre_text(self) -> Optional[str]:
        return self._pre_text

    @pre_text.setter
    def pre_text(self, text: Optional[str]) -> None:
        self._pre_text = Menu._get_property_text(text)

    @property
    def post_text(self) -> Optional[str]:
        return self._post_text

    @post_text.setter
    def post_text(self, text: Optional[str]) -> None:
        self._post_text = Menu._get_property_text(text)

    @property
    def prompt(self) -> str:
        return self._prompt

    @prompt.setter
    def prompt(self, text: str):
        self._prompt = Menu._get_property_text(text, none_default="", empty_default="")

    @staticmethod
    def _get_property_text(text: str,
                           none_default: Optional[str] = None,
                           empty_default: Optional[str] = None) -> Optional[str]:
        txt = none_default if text is None else str(text)
        if (txt is not None) and (len(txt) < 1):
            txt = empty_default
        return txt

    @property
    def choice_index(self) -> Optional[int]:
        return None if self._choice_indexes is None else self._choice_indexes[0]

    @property
    def choice_number(self) -> Optional[int]:
        return None if self.choice_index is None else self.choice_index + 1

    @property
    def choice_text(self) -> Optional[str]:
        return None if self.choice_index is None else self.menu_items[self.choice_index]

    @property
    def choice_indexes(self) -> Optional[Tuple[int, ...]]:
        if self._choice_indexes is None:
            return None
        return tuple(list(copy.deepcopy(self._choice_indexes)))

    @property
    def choice_numbers(self) -> Optional[Tuple[int, ...]]:
        return None if self._choice_indexes is None else tuple([idx + 1 for idx in self._choice_indexes])

    @property
    def choice_texts(self) -> Optional[Tuple[str, ...]]:
        return None if self._choice_indexes is None else tuple([self.menu_items[idx] for idx in self._choice_indexes])

    # User Choice(s) ###################################################################################################

    def choose(self) -> None:
        self._choose(multi_choices=False,
                     multi_duplicates=False,
                     multi_sort=False)

    def choose_multiple(self,
                        duplicates: bool = False,
                        sort: bool = True) -> None:
        self._choose(multi_choices=True,
                     multi_duplicates=duplicates,
                     multi_sort=sort)

    def _choose(self,
                multi_choices: bool,
                multi_duplicates: bool,
                multi_sort: bool) -> None:
        self._display_menu()
        prompt = self._build_prompt()
        self._choice_indexes = None
        error_count = 0
        while self._choice_indexes is None:
            try:
                self._user_choose(prompt=prompt,
                                  multi_choices=multi_choices,
                                  multi_duplicates=multi_duplicates,
                                  multi_sort=multi_sort)
            except ValueError:
                self._display_choice_error(multi_choices=multi_choices,
                                           multi_duplicates=multi_duplicates)
                error_count += 1
                if error_count >= Menu.CONFIG["error_count_redisplay"]:
                    self._display_menu()
                    error_count = 0

    def _user_choose(self,
                     prompt: str,
                     multi_choices: bool,
                     multi_duplicates: bool,
                     multi_sort: bool) -> None:
        multi_sep = Menu.CONFIG["multiple_choice_separator"]
        user_choice = input(prompt)
        if (not multi_choices) or (multi_sep not in user_choice):
            # single item choice
            try:
                user_choice = int(user_choice) - 1
            except TypeError:
                raise ValueError()
            if not 0 <= user_choice < len(self.menu_items):
                raise ValueError()
            self._choice_indexes = (user_choice,)
        else:
            # multiple item choices
            temp_user_choices = [int(choice) - 1 for choice in user_choice.split(multi_sep)]
            user_choices = []
            for choice in temp_user_choices:
                if not 0 <= choice < len(self.menu_items):
                    raise ValueError()
                if (multi_duplicates) or (choice not in user_choices):
                    user_choices.append(choice)
            if multi_sort:
                user_choices.sort()
            self._choice_indexes = tuple(user_choices)

    # Display ##########################################################################################################

    def _display_menu(self) -> None:
        Menu._display_text(self.title, Menu.THEME["title"]["highlight"])
        Menu._display_text(self.pre_text, Menu.THEME["pre_text"]["highlight"])
        self._display_items()
        Menu._display_text(self.post_text, Menu.THEME["post_text"]["highlight"])

    @staticmethod
    def _display_text(text: str, highlight_theme: Dict) -> None:
        if text is None:
            return
        txt = Menu._highlight(text, **highlight_theme)
        print(f"{txt}\n")

    def _display_items(self) -> None:
        for index, item in enumerate(self.menu_items):
            Menu._display_item(index, item)
        print()

    @classmethod
    def _display_item(cls, index, item):
        num_theme = Menu.THEME["item_num"]
        num_format = f" >{num_theme['num_width']}"
        sep_theme = Menu.THEME["item_sep"]
        item_theme = Menu.THEME["item"]
        idx = Menu._highlight(f"{index + 1:{num_format}}", **num_theme["highlight"])
        idx_sep = Menu._highlight(f"{sep_theme['char']}", **sep_theme["highlight"])
        itm = Menu._highlight(f"{item}", **item_theme["highlight"])
        print(f"{idx}{idx_sep} {itm}")

    def _build_prompt(self) -> str:
        prompt_theme = Menu.THEME["prompt"]
        sep_theme = Menu.THEME["prompt_sep"]
        prmpt = Menu._highlight(f"{self.prompt}", **prompt_theme["highlight"])
        sep = Menu._highlight(f"{sep_theme['char']}", **sep_theme["highlight"])
        return f"{prmpt}{sep} "

    def _display_choice_error(self,
                              multi_choices: bool,
                              multi_duplicates: bool) -> None:
        error_theme = Menu.THEME["error"]
        txt_theme = Menu.THEME["error_text"]
        range_theme = Menu.THEME["error_range"]

        # Start error message
        msg = ""
        if not multi_choices:
            msg += Menu._highlight("INVALID CHOICE:", **error_theme["highlight"])
            msg += " "
            msg += Menu._highlight("Please choose a valid item between ", **txt_theme["highlight"])
        else:
            msg += Menu._highlight("INVALID CHOICE(S):", **error_theme["highlight"])
            msg += " "
            msg += Menu._highlight("Please choose valid item(s)")
            if not multi_duplicates:
                msg += Menu._highlight(", without duplicates,", **txt_theme["highlight"])
            msg += Menu._highlight(" between ", **txt_theme["highlight"])

        # build range of valid values
        msg += Menu._highlight("1", **range_theme["highlight"])
        msg += Menu._highlight(" and ", **txt_theme["highlight"])
        msg += Menu._highlight(f"{len(self.menu_items)}", **range_theme["highlight"])
        print(f"\n{msg}\n")

    @classmethod
    def _highlight(cls, text, *args, **kwargs):
        if not cls.CONFIG["ansi_support"]:
            return text
        return Menu._tiny_highlight(text=text, *args, **kwargs)

    @staticmethod
    # See Tiny ANSI: https://gitlab.com/joeysbytes-python-snippets/tiny-ansi
    def _tiny_highlight(text: str, fg: Optional[int] = None, bg: Optional[int] = None,
                   attrib: Optional[str] = None) -> str:
        start = "\033[";
        end = "\033[";
        attributes = [] if attrib is None else [a.lower().strip() for a in attrib.split(",")]
        if (fg is not None) and (0 <= fg <= 15): start += f"{fg % 8 + 30 + (60 * (fg > 7))};"; end += "39;"
        if (bg is not None) and (0 <= bg <= 15): start += f"{bg % 8 + 40 + (60 * (bg > 7))};"; end += "49;"
        for a in attributes:
            if a in ("b", "bold", "bright"):
                start += "22;1;"; end += "22;";
            elif a in ("i", "ital", "italic"):
                start += "3;"; end += "23;"
            elif a in ("u", "und", "underline"):
                start += "4;"; end += "24;"
            elif a in ("d", "dim", "f", "faint"):
                start += "22;2;"; end += "22;"
            elif a in ("n", "nrml", "norm", "normal"):
                start += "22;"; end += "22;"
            elif a in ("l", "blink"):
                start += "5;"; end += "25;"
            elif a in ("s", "strike", "strikethru", "strikethrough"):
                start += "9;"; end += "29;"
            elif a in ("r", "rev", "reverse"):
                start += "7;"; end += "27;"
        start = f"{start[:-1]}m";
        end = f"{end[:-1]}m";
        return f"{start}{text}{end}"
