import pytest
import data
from Menu import Menu


@pytest.fixture(autouse=True)
def setup_and_teardown():
    Menu.CONFIG["ansi_support"] = False
    yield
    Menu.CONFIG["ansi_support"] = True


@pytest.mark.parametrize("args, expected", (
    ({}, f"{data.ITEMS_DISPLAYED}\n"),
    ({"title": data.TITLE}, f"{data.TITLE}\n\n{data.ITEMS_DISPLAYED}\n"),
    ({"pre_text": data.PRE_TEXT}, f"{data.PRE_TEXT}\n\n{data.ITEMS_DISPLAYED}\n"),
    ({"post_text": data.POST_TEXT}, f"{data.ITEMS_DISPLAYED}\n{data.POST_TEXT}\n\n"),
    ({"title": data.TITLE, "pre_text": data.PRE_TEXT}, f"{data.TITLE}\n\n{data.PRE_TEXT}\n\n{data.ITEMS_DISPLAYED}\n"),
    ({"title": data.TITLE, "post_text": data.POST_TEXT}, f"{data.TITLE}\n\n{data.ITEMS_DISPLAYED}\n{data.POST_TEXT}\n\n"),
    ({"title": data.TITLE, "pre_text": data.PRE_TEXT, "post_text": data.POST_TEXT},
     f"{data.TITLE}\n\n{data.PRE_TEXT}\n\n{data.ITEMS_DISPLAYED}\n{data.POST_TEXT}\n\n"),
    ({"pre_text": data.PRE_TEXT, "post_text": data.POST_TEXT}, f"{data.PRE_TEXT}\n\n{data.ITEMS_DISPLAYED}\n{data.POST_TEXT}\n\n")
))
def test_display_menu(capsys, args, expected):
    menu = Menu(data.ITEMS_INPUT, **args)
    menu._display_menu()
    captured = capsys.readouterr()
    actual = captured.out
    assert actual == expected


def test_build_prompt_default():
    expected = "Enter choice: "
    menu = Menu(data.ITEMS_INPUT)
    actual = menu._build_prompt()
    assert actual == expected
