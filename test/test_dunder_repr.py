import data
from Menu import Menu


def build_repr(items=data.ITEMS_STORED,
               title=None,
               prompt="Enter choice",
               pre_text=None,
               post_text=None) -> str:
    repr_text = "Menu("
    repr_text += "items=" + repr(items)
    repr_text += ", title=" + repr(title)
    repr_text += ", prompt=" + repr(prompt)
    repr_text += ", pre_text=" + repr(pre_text)
    repr_text += ", post_text=" + repr(post_text)
    repr_text += ")"
    return repr_text


def test_defaults():
    expected = build_repr()
    menu_1 = Menu(data.ITEMS_STORED)
    assert repr(menu_1) == expected
    menu_2 = eval(repr(menu_1))
    assert menu_2 == menu_1
    assert repr(menu_2) == expected


def test_all_properties():
    expected = build_repr(items=data.ITEMS_STORED,
                          title=data.TITLE,
                          prompt=data.PROMPT,
                          pre_text=data.PRE_TEXT,
                          post_text=data.POST_TEXT)
    menu_1 = Menu(data.ITEMS_STORED,
                  title=data.TITLE,
                  prompt=data.PROMPT,
                  pre_text=data.PRE_TEXT,
                  post_text=data.POST_TEXT)
    assert repr(menu_1) == expected
    menu_2 = eval(repr(menu_1))
    assert menu_2 == menu_1
    assert repr(menu_2) == expected
