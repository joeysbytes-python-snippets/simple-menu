import pytest
import data
from Menu import Menu


@pytest.mark.parametrize("items", (list(), tuple(), ""))
def test_empty_items(items):
    with pytest.raises(ValueError) as ve:
        Menu(items)
    assert str(ve.value) == "ERROR: Menu item list cannot be empty"


@pytest.mark.parametrize("items", (-1, None, True, 3.14))
def test_invalid_items(items):
    with pytest.raises(TypeError) as te:
        Menu(items)


def test_items_setter():
    menu = Menu(data.ITEMS_INPUT)
    assert data.ITEMS_STORED == menu.menu_items


def test_items_getter():
    # verify menu.items is returning a copy of the items
    menu = Menu(data.ITEMS_INPUT)
    assert id(menu.menu_items) != id(menu._menu_items)
    assert menu.menu_items == data.ITEMS_STORED
