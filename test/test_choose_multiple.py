import pytest
import data
from Menu import Menu


@pytest.fixture(autouse=True)
def setup_and_teardown():
    Menu.CONFIG["ansi_support"] = False
    menu = Menu(data.ITEMS_INPUT, prompt=data.PROMPT)
    yield menu
    Menu.CONFIG["ansi_support"] = True


# Single choice ########################################################################################################

@pytest.mark.parametrize("user_choice", data.VALID_SINGLE_CHOICES)
def test_choose_multiple_single_valid(monkeypatch, setup_and_teardown, user_choice):
    menu = setup_and_teardown
    monkeypatch.setattr('builtins.input', lambda _: user_choice)
    expected = int(user_choice) - 1
    menu.choose_multiple()
    assert menu.choice_index == expected


@pytest.mark.parametrize("user_choice", data.INVALID_SINGLE_CHOICES)
def test_choose_multiple_single_invalid(monkeypatch, setup_and_teardown, user_choice):
    menu = setup_and_teardown
    monkeypatch.setattr('builtins.input', lambda _: user_choice)
    with pytest.raises(ValueError):
        menu._user_choose(prompt=data.PROMPT,
                          multi_choices=True,
                          multi_duplicates=True,
                          multi_sort=False)


# Multi-choice #########################################################################################################

@pytest.mark.parametrize("user_choice", data.INVALID_SINGLE_CHOICES)
def test_choose_multiple_invalid_item(monkeypatch, setup_and_teardown, user_choice):
    menu = setup_and_teardown
    user_multi_choice = f"{data.VALID_SINGLE_CHOICES[0]},{user_choice},{data.VALID_SINGLE_CHOICES[1]}"
    monkeypatch.setattr('builtins.input', lambda _: user_multi_choice)
    with pytest.raises(ValueError):
        menu._user_choose(prompt=data.PROMPT,
                          multi_choices=True,
                          multi_duplicates=True,
                          multi_sort=False)


def test_choose_multiple_allow_duplicates_no_sort(monkeypatch, setup_and_teardown):
    menu = setup_and_teardown
    expected = data.VALID_MULTI_CHOICE_INDEXES
    monkeypatch.setattr('builtins.input', lambda _: data.VALID_MULTI_CHOICE_INPUT)
    menu.choose_multiple(duplicates=True, sort=False)
    actual = menu.choice_indexes
    assert actual == expected


def test_choose_multiple_allow_duplicates_with_sort(monkeypatch, setup_and_teardown):
    menu = setup_and_teardown
    expected = data.VALID_MULTI_CHOICE_INDEXES_SORTED
    monkeypatch.setattr('builtins.input', lambda _: data.VALID_MULTI_CHOICE_INPUT)
    menu.choose_multiple(duplicates=True, sort=True)
    actual = menu.choice_indexes
    assert actual == expected


def test_choose_multiple_disallow_duplicates_no_sort(monkeypatch, setup_and_teardown):
    menu = setup_and_teardown
    expected = data.VALID_MULTI_CHOICE_INDEXES_NO_DUPES
    monkeypatch.setattr('builtins.input', lambda _: data.VALID_MULTI_CHOICE_INPUT)
    menu.choose_multiple(duplicates=False, sort=False)
    actual = menu.choice_indexes
    assert actual == expected


def test_choose_multiple_disallow_duplicates_with_sort(monkeypatch, setup_and_teardown):
    menu = setup_and_teardown
    expected = data.VALID_MULTI_CHOICE_INDEXES_NO_DUPES_SORTED
    monkeypatch.setattr('builtins.input', lambda _: data.VALID_MULTI_CHOICE_INPUT)
    menu.choose_multiple(duplicates=False, sort=True)
    actual = menu.choice_indexes
    assert actual == expected
