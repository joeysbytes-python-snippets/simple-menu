# Select Single Item
from Menu import Menu

menu = Menu(["Linux", "Windows", "MacOS"], title="Operating Systems", prompt="Choose your operating system")
menu.choose()
print(f"You chose: {menu.choice_text}")
