from Menu import Menu

Menu.CONFIG["ansi_support"] = True

title = "Dinner Menu"
# title = None
prompt = "What sounds good?"
# prompt = None
pre_text = "What animal would you like prepared for dinner?"
# pre_text = None
post_text = "Remember to ask about our wine specials!"
# post_text = None
menu_items = ("Kitten", "Puppy", "Bunny", "Chick", "Hamster", "Gerbil",
              "Goldfish", "Calf", "Piglet", "Penguin", "Bear Cub", "Canary")


def main():
    menu = Menu(items=menu_items, title=title, prompt=prompt, pre_text=pre_text, post_text=post_text)

    allow_multi = input("Do you want to enter multiple choices? (y/n): ")
    if allow_multi.lower() == "y":
        allow_dupes = input("Do you want to allow duplicate entries? (y/n): ")
        multi_duplicates = True if allow_dupes.lower() == "y" else False
        allow_sort = input("Do you want to sort choices(y/n): ")
        multi_sort = True if allow_sort.lower() == "y" else False
        menu.choose_multiple(duplicates=multi_duplicates, sort=multi_sort)
    else:
        menu.choose()

    print("\nMenu String:")
    print(menu)
    print("\nMenu Repr:")
    print(repr(menu))


if __name__ == "__main__":
    main()
