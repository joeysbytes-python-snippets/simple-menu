import pytest
import data
from Menu import Menu


@pytest.mark.parametrize("prompt", (None, ""))
def test_prompt_empty(prompt):
    expected = ""
    menu = Menu(data.ITEMS_INPUT, prompt=prompt)
    assert menu.prompt == expected


@pytest.mark.parametrize("prompt", (-1, True, 3.14, "Prompt"))
def test_prompt(prompt):
    menu = Menu(data.ITEMS_INPUT, prompt=prompt)
    assert menu.prompt == str(prompt)


def test_prompt_default():
    expected = "Enter choice"
    menu = Menu(data.ITEMS_INPUT)
    assert menu.prompt == expected
